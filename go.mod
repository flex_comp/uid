module gitlab.com/flex_comp/uid

go 1.16

require (
	github.com/bwmarrin/snowflake v0.3.0
	github.com/fsnotify/fsnotify v1.5.1 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/pelletier/go-toml v1.9.4 // indirect
	github.com/spf13/cast v1.4.1 // indirect
	gitlab.com/flex_comp/comp v0.1.3
	gitlab.com/flex_comp/conf v0.1.0 // indirect
	gitlab.com/flex_comp/log v0.1.0
	gitlab.com/flex_comp/redis v0.1.5
	gitlab.com/flex_comp/util v0.0.0-20210913121727-7fa21834c3e9
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.19.1 // indirect
	golang.org/x/sys v0.0.0-20210910150752-751e447fb3d0 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/ini.v1 v1.63.0 // indirect
)
