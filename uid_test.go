package uid

import (
	"gitlab.com/flex_comp/comp"
	"testing"
	"time"
)

func TestUID(t *testing.T) {
	t.Run("test_uid", func(t *testing.T) {
		_ = comp.Init(map[string]interface{}{
			"config": "./test/server.json",
		})

		go func() {
			ticker := time.NewTicker(time.Second)
			defer func() {
				ticker.Stop()
			}()

			for range ticker.C {
				t.Log(Gen(), " ", GenString())
			}
		}()

		ch := make(chan bool, 1)
		go func() {
			<-time.After(time.Second * 10)
			ch <- true
		}()

		_ = comp.Start(ch)
	})
}
