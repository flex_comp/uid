package uid

var (
	nodeIdScriptName          = "_get_node_id"
	nodeIdKeepaliveScriptName = "_keepalive_node_id"
	nodePoolKey               = "node:pool"
	nodePoolCapacity          = 1024
	nodeExpireMillSec         = 10000
	nodeKeepaliveInterval     = 3000
)

var nodeIdScript = `local id_pool = KEYS[1]
local cap = ARGV[1]
local time_stamp = tonumber(ARGV[2])
local expire = tonumber(ARGV[3])

local len = redis.call("ZCARD", id_pool)
local diff = cap - len
local tbl = {}
for i = 1, diff do
    table.insert(tbl, 0)
    table.insert(tbl, tostring(i + len - 1))
end

if #tbl > 0 then
    redis.call("ZADD", id_pool, unpack(tbl))
end

local node_ids = redis.call("ZRANGEBYSCORE", id_pool, "0", "0", "LIMIT", 0, 1)
if #node_ids == 0 then
    node_ids = redis.call("ZRANGEBYSCORE", id_pool, "0", "(" .. tostring(time_stamp - expire), "LIMIT", 0, 1)
end

if #node_ids == 0 then
    return -1
end

redis.call("ZADD", id_pool, time_stamp, node_ids[1])
return tonumber(node_ids[1])`

var nodeIdKeepaliveScript = `local id_pool = KEYS[1]
local node_id = ARGV[1] 
local time_stamp = tonumber(ARGV[2])
redis.call("ZADD", id_pool, "XX", time_stamp, node_id)`
