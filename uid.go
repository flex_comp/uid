package uid

import (
	"errors"
	"github.com/bwmarrin/snowflake"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/redis"
	"gitlab.com/flex_comp/util"
	"time"
)

var (
	ins *UID
)

type UID struct {
	node *snowflake.Node
}

func init() {
	ins = new(UID)
	_ = comp.RegComp(ins)
}

func (u *UID) Init(map[string]interface{}, ...interface{}) error {
	return nil
}

func (u *UID) Start(...interface{}) error {
	redis.LoadScriptRaw(nodeIdKeepaliveScriptName, 1, nodeIdKeepaliveScript)
	redis.LoadScriptRaw(nodeIdScriptName, 1, nodeIdScript)
	rep, err := redis.Script(nodeIdScriptName, nodePoolKey, nodePoolCapacity, time.Now().UnixNano()/1e6, nodeExpireMillSec)
	if err != nil {
		return err
	}

	nodeId := util.ToInt64(rep)
	if nodeId == -1 {
		return errors.New("无可用节点ID")
	}

	go func() {
		for range time.Tick(time.Millisecond * time.Duration(nodeKeepaliveInterval)) {
			_, err := redis.Script(nodeIdKeepaliveScriptName, nodePoolKey, util.ToString(nodeId), time.Now().UnixNano()/1e6)
			if err != nil {
				log.ErrorF("节点ID续期失败: %s", err)
				//os.Exit(-1)
			}
		}
	}()

	log.Info("获取到节点ID: ", nodeId)
	u.node, err = snowflake.NewNode(nodeId)
	if err != nil {
		return err
	}
	return nil
}

func (u *UID) UnInit() {
}

func (u *UID) Name() string {
	return "unique-id"
}

func Gen() int64 {
	return ins.Gen()
}

func (u *UID) Gen() int64 {
	return u.node.Generate().Int64()
}

func GenString() string {
	return ins.GenString()
}

func (u *UID) GenString() string {
	return u.node.Generate().String()
}
